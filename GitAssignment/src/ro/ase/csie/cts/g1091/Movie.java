package ro.ase.csie.cts.g1091;

public class Movie implements NetflixWatchable {
	
	// Private fields
	private String genre;
	private String originCountry;
	private float duration;
	
	/// Constructor
	public Movie(String genre, String originCountry, float duration) {
		super();
		this.genre = genre;
		this.originCountry = originCountry;
		this.duration = duration;
	}

	
	// Methods
	
	@Override
	public void checkNetflixAvailability() {
		StringBuffer status = new StringBuffer();
		
		status.append("Yes, this ");
		status.append(this.genre);
		status.append(" movie is available on Netflix and it is ");
		status.append(this.duration);
		status.append(" minutes long!");
		
		System.out.println(status);
		
	}
	
	
	// Getters and Setters
	
	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	

}
