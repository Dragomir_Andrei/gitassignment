package ro.ase.csie.cts.g1091;

public interface NetflixWatchable {
	
	public void checkNetflixAvailability();

}
