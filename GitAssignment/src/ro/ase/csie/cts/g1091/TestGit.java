package ro.ase.csie.cts.g1091;

public class TestGit {

	public static void main(String[] args) {
		
		System.out.println
		("Hello Git! The name of the license project is Multimedia Web solution for entertainment");
		
		
		Movie Parasite = new Movie("Comedy", "South Korea", 132);
		
		
		// Testing the getters
		System.out.println("The genre of the movie is: " + Parasite.getGenre());
		System.out.println("The duration of the movie is: " + Parasite.getDuration());
		System.out.println("The country of origin for the movie is: "+ Parasite.getOriginCountry());
		
		// Changing movie genre
		
		Parasite.setGenre("Drama");
		
		System.out.println();
		
		Parasite.checkNetflixAvailability();

	}

}
